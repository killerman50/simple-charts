import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XygapComponent } from './xygap.component';

describe('XygapComponent', () => {
  let component: XygapComponent;
  let fixture: ComponentFixture<XygapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XygapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XygapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
