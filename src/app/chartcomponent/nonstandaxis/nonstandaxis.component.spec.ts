import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonstandaxisComponent } from './nonstandaxis.component';

describe('NonstandaxisComponent', () => {
  let component: NonstandaxisComponent;
  let fixture: ComponentFixture<NonstandaxisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonstandaxisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonstandaxisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
