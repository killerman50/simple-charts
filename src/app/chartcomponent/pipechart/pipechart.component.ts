import { Component, OnInit, NgZone, AfterViewInit, OnDestroy } from '@angular/core';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

am4core.useTheme(am4themes_animated);

@Component({
    selector: 'app-pipechart',
    templateUrl: './pipechart.component.html',
    styleUrls: ['./pipechart.component.scss']
})
export class PipechartComponent implements OnInit , AfterViewInit, OnDestroy {

    private chart: am4charts.PieChart;

    constructor(private zone: NgZone) { }

    ngOnInit() {
    }

    ngAfterViewInit() {

        this.zone.runOutsideAngular(() => {
            const chart = am4core.create('pipediv', am4charts.PieChart);

            chart.paddingRight = 20;


            chart.data = [{
                country: 'Lithuania',
                litres: 501.9
            }, {
                country: 'Czech Republic',
                litres: 301.9
            }, {
                country: 'Ireland',
                litres: 201.1
            }, {
                country: 'Germany',
                litres: 165.8
            }, {
                country: 'Australia',
                litres: 139.9
            }, {
                country: 'Austria',
                litres: 128.3
            }, {
                country: 'UK',
                litres: 99
            }, {
                country: 'Belgium',
                litres: 60
            }, {
                country: 'The Netherlands',
                litres: 50
            }];

            const pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = 'litres';
            pieSeries.dataFields.category = 'country';

            this.chart = chart;
        });
    }


    ngOnDestroy() {
        this.zone.runOutsideAngular(() => {
            if (this.chart) {
                this.chart.dispose();
            }
        });
    }

}
