import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StakedchartsComponent } from './stakedcharts.component';

describe('StakedchartsComponent', () => {
  let component: StakedchartsComponent;
  let fixture: ComponentFixture<StakedchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StakedchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StakedchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
