import { Component, OnInit } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';

@Component({
  selector: 'app-stakedcharts',
  templateUrl: './stakedcharts.component.html',
  styleUrls: ['./stakedcharts.component.scss']
})
export class StakedchartsComponent implements OnInit {

  public Chart: GoogleChartInterface = {
    chartType: 'ColumnChart',
    dataTable: [
      ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', { role: 'annotation' }],
      ['2010', 100, 50, 60, ''],
      ['2020', 100, 100, 70, ''],
      ['2030', 100, 100, 100, '']
    ],
    options: { 'isStacked': true }
  };

  constructor() { }

  ngOnInit() {
  }

}
