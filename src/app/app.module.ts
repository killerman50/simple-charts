import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { GoogleChartsModule } from 'angular-google-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { XychartComponent } from './chartcomponent/xychart/xychart.component';
import { XygapComponent } from './chartcomponent/xygap/xygap.component';
import { PipechartComponent } from './chartcomponent/pipechart/pipechart.component';
import { NonstandaxisComponent } from './chartcomponent/nonstandaxis/nonstandaxis.component';
import { FblComponent } from './chartcomponent/fbl/fbl.component';
import { SlsComponent } from './chartcomponent/sls/sls.component';
import { StakedchartsComponent } from './chartcomponent/stakedcharts/stakedcharts.component';

import {Ng2GoogleChartsModule} from 'ng2-google-charts';

@NgModule({
  declarations: [
    AppComponent,
    XychartComponent,
    XygapComponent,
    PipechartComponent,
    NonstandaxisComponent,
    FblComponent,
    SlsComponent,
    StakedchartsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  //  GoogleChartsModule.forRoot(),
    Ng2GoogleChartsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
